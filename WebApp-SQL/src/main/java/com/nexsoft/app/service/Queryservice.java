package com.nexsoft.app.service;

import java.sql.*;
import java.util.ArrayList;

public class Queryservice {

    public static ArrayList<ArrayList<String>> doSelect(ResultSet result) throws SQLException{
        ResultSetMetaData meta= result.getMetaData();
                
        ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();     
        while (result.next()) {
            ArrayList<String> inner = new ArrayList<String>();
            for (int i = 1; i < meta.getColumnCount()+1; i++) {
                inner.add(result.getString(i));
            }
            outer.add(inner);
        }
        return outer;
    }

    public static ArrayList<String> getColumList(ResultSet result) throws SQLException{
        ResultSetMetaData meta= result.getMetaData();
        int columns_num= meta.getColumnCount();
        ArrayList<String> column_name = new ArrayList<String>();
        for (int j = 1; j < columns_num+1; j++) {
            column_name.add(meta.getColumnName(j));
        }
        return column_name;
    }
    
}
