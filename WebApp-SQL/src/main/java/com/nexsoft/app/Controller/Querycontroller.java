package com.nexsoft.app.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.nexsoft.app.dao.Querydao;
import com.nexsoft.app.model.Query;
import com.nexsoft.app.service.Queryservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class Querycontroller {

    @RequestMapping(value ="/save", method = RequestMethod.POST)
    public ModelAndView add(@ModelAttribute("query") String query) throws ClassNotFoundException, SQLException{
        ModelAndView mv = new ModelAndView("index");
        Statement ps = Querydao.establishConnection();
        String subString = query.substring(0,6);
        switch (subString) {
            case "select":
                ResultSet result = ps.executeQuery(query);
                ArrayList<ArrayList<String>> data= Queryservice.doSelect(result);
                ArrayList<String> columnsnames = Queryservice.getColumList(result);
                mv.addObject("columnsnames", columnsnames);
                mv.addObject("data", data);
                break;

            default:
                ps.executeUpdate(query);
                break;
        }
        mv.addObject("Query", new Query());
        return mv;
    }

    @GetMapping(value="/home")
    public ModelAndView queryInput() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("Query", new Query());
        return mv;
    }
}
