<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Add Name</h1>
	<form:form action = "save" modelAttribute="Query">
		<form:textarea path = "query" rows = "5" cols = "30" />
		<button type = "submit">Save</button>
	</form:form>
        <table border="1">
            <tr>
                <c:forEach items="${columnsnames}" var="c">
                    <th>${c}</th>
                </c:forEach>    
            </tr>
        <c:forEach items="${data}" var="e">
            <tr>
            <c:forEach items="${e}" var="b">
                <td>${b}</td>
            </c:forEach>
            </tr>
        </c:forEach>
        </table>
</body>
</html>